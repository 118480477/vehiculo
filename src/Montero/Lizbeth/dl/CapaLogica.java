package Montero.Lizbeth.dl;

import Montero.Lizbeth.bl.Motor;
import Montero.Lizbeth.bl.Vehiculo;

import java.util.ArrayList;

public class CapaLogica {
    private ArrayList<Vehiculo> carros;
    private ArrayList<Motor> motores;

    public CapaLogica() {
        carros = new ArrayList<>();
        motores = new ArrayList<>();
    }

    public String agregarVehiculo(Vehiculo vehiculo) {
        carros.add(vehiculo);
        return "registrado correctamente";
    }
    public String agregarMotor(Motor motor) {
        motores.add(motor);
        return "Se agrego correctamente";
    }

    public String[] listarVehiculos() {
        String[] infovehiculos = new String[carros.size()];
        for (int i = 0; i < carros.size(); i++) {
            infovehiculos[i] = carros.get(i).propio();
        }
        return infovehiculos;

    }
    public String[] listarMotores() {
        String [] infomotores = new String[motores.size()];
        for (int i=0; i<motores.size();i++){
            infomotores[i]= motores.get(i).toString();
        }
        return infomotores;
    }
    public String[] listarVehiculosConMotor(){
        String[]info= new String[carros.size()];
        for(int i=0; i<carros.size(); i++){
            info[i]=motores.toString();
        }
        return info;
    }
    public String[] listarVehiculosMotores() {
        String[] infovehiculosMotores = new String[carros.size()];
        for (int i = 0; i < carros.size(); i++) {
            infovehiculosMotores[i] = carros.get(i).toString();
        }
        return infovehiculosMotores;
    }

    public int buscarMotor(String serie) {
        int posicion = 0;
        for (Motor motorS : motores) {
            if (serie.equals(motorS.getSerie())) {
                return posicion;
            }
            posicion++;
        }
        return -1;
    }

    public int buscarVehiculo(String placa) {
        int posicion = 0;
        for (Vehiculo carrro : carros) {
            if (placa.equals(carrro.getPlaca())) {
                return posicion;
            }
            posicion++;
        }
        return -1;
    }
    public void agregarMotorAVehiculo(int posV, int posM) {
        Motor motornuevo = motores.get(posM);
        carros.get(posV).setMotor(motornuevo);
    }

    public String getMotor(int posicion) {
        return motores.get(posicion).toString();
    }

    public String getCarro(int posicion) {
        return carros.get(posicion).toString();
    }
}// FIN CLASS
