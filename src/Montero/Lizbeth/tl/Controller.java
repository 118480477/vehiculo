package Montero.Lizbeth.tl;

import Montero.Lizbeth.bl.Motor;
import Montero.Lizbeth.bl.Vehiculo;
import Montero.Lizbeth.dl.CapaLogica;

public class Controller {
    private CapaLogica logica = new CapaLogica();

    public String agregarVehiculo(String placa, String marca, String color) {
        Vehiculo carro = new Vehiculo(placa, marca, color);
        return logica.agregarVehiculo(carro);
    }

    public String ageragarVehiculoConMotor(String placa, String marca, String color, String serie, String cilindros) {
        Motor nuevoMotor = new Motor(serie, cilindros);
        Vehiculo carro = new Vehiculo(placa, marca, color, nuevoMotor);
        logica.agregarVehiculo(carro);
        return logica.agregarVehiculo(carro);
    }
    public int buscarMotor(String serie) {
        int encontrado = logica.buscarMotor(serie);
        return encontrado;
    }
    public int buscarVehiculo(String placa) {
        int encontrado = logica.buscarVehiculo(placa);
        return encontrado;
    }
    public String agregarMotor(String serie, int cilindro) {
        Motor motor = new Motor(serie, cilindro);
        return logica.agregarMotor(motor);
    }

    public String[] listarMotores() {
        return logica.listarMotores();
    }

    public String[] listarVehiculo() {
        return logica.listarVehiculos();
    }
    public String[] imprimirVehiculosMotores(){
        return logica.listarVehiculosMotores();
    }

    public boolean asociarMotorVehiculo(String placa, String serie) {
        int posMotor = logica.buscarVehiculo(serie);
        if (posMotor >= 0) {
            int posV = logica.buscarMotor(serie);
            if (posV >= 0) {
                logica.agregarMotorAVehiculo(posV, posMotor);
                return true;
            }
        }
        return false;

    }
}
