package Montero.Lizbeth.ui;

import Montero.Lizbeth.tl.Controller;

import java.io.*;

public class Main {
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    static Controller administrador = new Controller();

    public static void main(String[] args) throws IOException {
        mostrarMenu();
    }

    public static void registrarMotor() throws IOException {
        out.println("Digite la serie del motor");
        String serie = in.readLine();
        out.println("Digite los cilindros del motor");
        int cilindros = Integer.parseInt(in.readLine());
        if (administrador.buscarMotor(serie)==-1){
            administrador.agregarMotor(serie,cilindros);
            out.println("El motor se ha resgistrado correctamente");
        } else {
            out.println("El Motor : " + serie + " ya existe.");
        }
    }

    public static void listarMotor() {
            String[] infoMotor = administrador.listarMotores();
            for (int i = 0; i < infoMotor.length; i++) {
                out.println(infoMotor[i]);
            }
        }

    public static void registrarVehiculo() throws IOException {
        out.println("Digite la placa del vheiculo");
        String placa = in.readLine();
        out.println("Digite la marca del vehiculo");
        String marca = in.readLine();
        out.println("Digite el color del vehiculo");
        String color = in.readLine();
        if (administrador.buscarVehiculo(placa)==-1) {
            administrador.agregarVehiculo(placa,marca,color);
            out.println("El Vehiculo se ha resgistrado correctaente");
        } else {
            out.println("\"El vehiculo con placa: " + placa + " ya existe.");
        }
    }

    public static void listarVehiculo() {
        String[] infoVehiculo = administrador.listarVehiculo();
        for (int i = 0; i < infoVehiculo.length; i++) {
            out.println(infoVehiculo[i]);
        }
    }

    public static void agregarMotorVehiculo() throws IOException{
        out.print("Ingrese el número de serie Del motor: ");
        String serie = in.readLine();
        out.print("Digite el número de Placa del vehículo: ");
        String placa = in.readLine();

        if(administrador.asociarMotorVehiculo(serie, placa)){
            out.println("Se asoció el Motor al vehículo de manera correcta");
        } else {
            out.println("No se pudo asociar el vehiculo al motor, debido a que alguno de los 2 no existe");
        }

    }
    public static void ListarVehiculosMotores() {
        String[] infoVehiculoMotores = administrador.imprimirVehiculosMotores();
        for (int i = 0; i < infoVehiculoMotores.length; i++) {
            out.println(infoVehiculoMotores[i]);
        }
    }


    static void mostrarMenu() throws IOException {
        int opcion = -1;
        do {
            System.out.println("Menú");
            System.out.println("1. Registrar Motor");
            System.out.println("2. Listar Motor");
            System.out.println("3. Registrar vehiculo");
            System.out.println("4. Listar vehiculo");
            System.out.println("5. Registrar Vehiculo con Motor");
            System.out.println("6. listar Vehiculo con Motor");
            System.out.println("0. Salir");
            opcion = seleccionarOpcion();
            procesarOpcion(opcion);
        } while (opcion != 0);
    }

    /**
     * Rutina (función) que retorna el valor ingresado por el usuario.
     *
     * @return devuelve la opción seleccionada
     */
    static int seleccionarOpcion() throws IOException {
        System.out.println("Digite la opción");
        return Integer.parseInt(in.readLine());

    }
    static void procesarOpcion(int pOpcion) throws IOException {
        switch (pOpcion) {
            case 0:
                System.out.println("¡Aplicación cerrada exitosamente!.");
                break;
            case 1:
                registrarMotor();
                break;
            case 2:
                listarMotor();
                break;
            case 3:
                registrarVehiculo();
                break;
            case 4:
                listarVehiculo();
                break;
            case 5:
                agregarMotorVehiculo();
                break;
            case 6:
                ListarVehiculosMotores();
                break;
            default:
                System.out.println("Opción inválida");
                break;
        }
    }
}

