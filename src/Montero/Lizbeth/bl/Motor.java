package Montero.Lizbeth.bl;

/**
 * se inicializan los atributos
 */
public class Motor {
    private String serie;
    private int cilindros;

    /**
     * Constructor con los parametros propios propios
     * @param serie tipo String  guarda la serie del vehiculo
     * @param cilindros tipo String guarda los cilindros del carro
     */
    public Motor(String serie, String cilindros) {
    }
    /**
     * Constructor con los parametros propios propios
     * @param serie tipo String  guarda la serie del vehiculo
     * @param cilindros tipo int guarda los cilindros del carro
     */
    public Motor(String serie, int cilindros) {
        this.serie = serie;
        this.cilindros = cilindros;
    }

    /**
     * Constructor por defecto.
     */
    public Motor() {
    }
    /**
     * Metodo Gettter de la variable serie es utilizado para obterner el atributo privado de la clase.
     *
     * @return devuelve  el serie  del motor ( lo vuleve publico) y de esta manera puede ser nombrado en otras calses.
     */
    public String getSerie() {
        return serie;
    }
    /**
     * Metodo set es utilizado para asignar valores a los atributos privados de una clase.
     * @param serie variable serie del motor ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setSerie(String serie) {
        this.serie = serie;
    }
    /**
     * Metodo Gettter de la variable clindros es utilizado para obterner el atributo privado de la clase.
     *
     * @return devuelve  los cilindros  del motor ( lo vuleve publico) y de esta manera puede ser nombrado en otras calses.
     */
    public int getCilindros() {
        return cilindros;
    }
    /**
     * Metodo set es utilizado para asignar valores a los atributos privados de una clase.
     * @param cilindros variable cilindros del motor ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setCilindros(int cilindros) {
        this.cilindros = cilindros;
    }
    /**
     * Metodo toString utilizado para imprimir la in información almacenada en un String asignado.
     * @return retorna todos los valores respectivos del objeto, en este caso el respectivo valor de cada atributo de la clase motor en un solo String.
     */
    @Override
    public String toString() {
        return "Motor{" +
                "serie='" + serie + '\'' +
                ", cilindros=" + cilindros +
                '}';
    }
}
