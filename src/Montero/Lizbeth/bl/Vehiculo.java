package Montero.Lizbeth.bl;

/**
 * @author Lizbeth Montero
 * @version 1.0.0
 * se inicializan los atributo
 */
public class Vehiculo {
    private String placa, marca, color;
    private Motor motor;

    /**
     * Contructor vacio
     */
    public Vehiculo() {
    }

    /**
     * constructor con los parametros propios de la clase
     * @param placa tipo String guarda la placa del carro
     * @param marca tipo String guarda la marca del carro
     * @param color tipo String guarda el color del carro
     */
    public Vehiculo(String placa, String marca, String color) {
        this.placa = placa;
        this.marca = marca;
        this.color = color;
    }
    /**
     * constructor con los parametros propios de la clase
     * @param placa tipo String guarda la placa del carro
     * @param marca tipo String guarda la marca del carro
     * @param color tipo String guarda el color del carro
     * @param motor tipo motor motor del carro
     */
    public Vehiculo(String placa, String marca, String color, Motor motor) {
        this.placa = placa;
        this.marca = marca;
        this.color = color;
        this.motor = motor;
    }
    /**
     * Metodo Gettter de la variable placa es utilizado para obterner el atributo privado de la clase.
     *
     * @return devuelve  la placa del carro ( lo vuleve publico) y de esta manera puede ser nombrado en otras calses.
     */
    public String getPlaca() {
        return placa;
    }
    /**
     * Metodo set es utilizado para asignar valores a los atributos privados de una clase.
     * @param placa variable placa del carro  ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setPlaca(String placa) {
        this.placa = placa;
    }
    /**
     * Metodo Gettter de la variable marca es utilizado para obterner el atributo privado de la clase.
     *
     * @return devuelve  la marca  del carro ( lo vuleve publico) y de esta manera puede ser nombrado en otras calses.
     */
    public String getMarca() {
        return marca;
    }
    /**
     * Metodo set es utilizado para asignar valores a los atributos privados de una clase.
     * @param marca variable marca del carro ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }
    /**
     * Metodo Gettter de la variable color es utilizado para obterner el atributo privado de la clase.
     *
     * @return devuelve  el color  del carro ( lo vuleve publico) y de esta manera puede ser nombrado en otras calses.
     */

    public String getColor() {
        return color;
    }
    /**
     * Metodo set es utilizado para asignar valores a los atributos privados de una clase.
     * @param color variable color del carro ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setColor(String color) {
        this.color = color;
    }
    /**
     * Metodo Gettter de la variable motor es utilizado para obterner el atributo privado de la clase.
     *
     * @return devuelve  el motor  del carro ( lo vuleve publico) y de esta manera puede ser nombrado en otras calses.
     */

    public Motor getMotor() {
        return motor;
    }
    /**
     * Metodo set es utilizado para asignar valores a los atributos privados de una clase.
     * @param motor variable motor del carro ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */

    public void setMotor(Motor motor) {
        this.motor = motor;
    }
    /**
     * Metodo toString utilizado para imprimir la in información almacenada en un String asignado.
     * @return retorna todos los valores respectivos del objeto, en este caso el respectivo valor de cada atributo de la clase vehiculo en un solo String.
     */
    @Override
    public String toString() {
        return "Vehiculo{" +
                "placa='" + placa + '\'' +
                ", marca='" + marca + '\'' +
                ", color='" + color + '\'' +
                ", motor=" + motor +
                '}';
    }
    /**
     * Metodo toString utilizado para imprimir la in información almacenada en un String asignado.
     * @return retorna los valores propios respectivos del objeto, en este caso el respectivo valor de cada atributo de la clase vehiculo en un solo String.
     */
    public String propio() {
        return "Vehiculo{" +
                "placa='" + placa + '\'' +
                ", marca='" + marca + '\'' +
                ", color='" + color + '\'' +
                '}';

    }
}// FIN CLASS.
